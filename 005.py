import json

class Cart:
    __data = {}
    __total_qty = 0
    __total_items = 0
    def __init__(self):
        self._quantity = 1
        self._discount = 0

    def addItem(self, item):
        if 'quantity' in item:
            pass
        else:
          #print(item)
          new_quantity = {'quantity':self._quantity}
          item.update(new_quantity)
          for k,v in item.items():
              self.__data[k] = v
          #print(self.__data)
        print(self.__data)
    
    def removeItem(self, remove_item):
        new_items = {key:val for key, val in self.__data.items() if val != remove_item}
        print("Data {} berhasil dihapus !".format(remove_item))
        print(self.__data)

    def addDiscount(self, discount = 0):
        afterDisc = (self.__data['price'] * discount) / 100
        print(afterDisc)

    def totalQuantity(self):
        if 'quantity' in self.__data:
            self.__total_qty += self.__data["quantity"]
            print("total quantity is : ", self.__total_qty)
    
    def totalItems(self):
        for key, value in self.__data.items():
            temp = [key,value]
            temp.append(self.__data)
        print("total item : ", len(temp))

    def showAll(self):
        print("Seluruh Data Pembelian")
        print("========================")
        print("Item_id \t Price \t Quantity")
        for value in self.__data:
            print("{} \t {} \t {}".format(value[0], value[1], value[2]))
        print(self.__data)

    def checkout(self):
        file_name = 'data.json'
        file_open = open(file_name, 'w+')
        with (file_open) as file:
            toJson = json.dumps(self.__data)
            file.write(toJson)

cart = Cart()
cart.addItem({ 'item_id': 1, 'price': 30000})
cart.addItem({ 'item_id': 3, 'price': 40000, 'quantity': 10})
cart.addItem({'item_id':1, "price":10000, "quantity":3})
cart.removeItem(1)
cart.addDiscount(50)
cart.totalItems()
cart.totalQuantity()
cart.addItem({ 'item_id': 4, 'price': 40000, 'quantity': 10})
cart.showAll()
cart.checkout()
