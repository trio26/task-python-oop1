import hashlib

class Hash:
    def md5(strings):
        encodeString = strings.encode('utf-8')
        encrypt = hashlib.md5(encodeString)
        print(encrypt.hexdigest())

    def sha1(strings):
        encodeString = strings.encode('utf-8')
        encrypt = hashlib.sha1(encodeString).hexdigest()
        print(encrypt)

    def sha256(strings):
        encodeString = strings.encode('utf-8')
        encrypt = hashlib.sha256(encodeString).hexdigest()
        print(encrypt)

    def sha512(strings):
        encodeString = strings.encode('utf-8')
        encrypt = hashlib.sha512(encodeString).hexdigest()
        print(encrypt)

Hash.md5('secret') # 5ebe2294ecd0e0f08eab7690d2a6ee69

Hash.sha1('secret') # e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4

Hash.sha256('secret') # 2bb80d537b1da3e38bd30361aa855686bde0eacd7162fef6a25fe97bf527a25b

Hash.sha512('secret')