from datetime import datetime
now = datetime.now()

class Log:

    def write_code(log_date, list_log):
        file_name = 'app.log'
        file = open(file_name, "a")
        for x, y in list_log.items():
            log_name = x.upper()
            # print("[{}] {}: {}.".format(log_date, log_name, y))
            log = "[{}] {}: {}.\n".format(log_date, log_name, y)
            file.write(log)
        
        print("Log berhasil ditulis !")
        file.close()

    def log_date():
        datetime = now.isoformat(timespec='microseconds')
        return datetime

        # LOG_FILENAME = 'app.log'

level = {'info': 'This is an information about something',
        'error': 'We can\'t divide any numbers by zero',
        'notice': 'Someone loves your status',
        'warn': 'Insufficient funds',
        'debug': 'This is debug message',
        'alert': 'Achtung! Achtung!',
        'critical': 'Medic!! We\'ve got critical damages',
        'emergency': 'System hung. Contact system administrator immediately!'
        }

Log.write_code(Log.log_date(), level)

# ch = logging.StreamHandler()
# ch.setLevel(logging.DEBUG)

# logger.addHandler(ch)


