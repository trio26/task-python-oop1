from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes 

class Cipher:

    def encrypt(words, pwd):
        if len(pwd) == 0:
            data = "Please type your password!"
        else:
            data = words.encode('utf-8')
            pwd = pwd.encode('utf-8')
            cipher_encrypt = AES.new(pwd, AES.MODE_CFB)
            data = cipher_encrypt.encrypt(data)
        return data
    
    def decrypt(encrypted_words, pwd):
        if len(pwd) == 0:
            data = "Please type your password!"
        else:
            cipher_decrypt = AES.new(pwd, AES.MODE_CFB)
            deciphered_bytes = cipher_decrypt.decrypt(encrypted_words)
            data = deciphered_bytes.decode('utf-8')
        return data


message = Cipher.encrypt('Ini tulisan rahasia', 'p4$$w0rd')

print(message)

decryptedMessage = Cipher.decrypt(message, 'p4$$w0rd')

print(decryptedMessage)

