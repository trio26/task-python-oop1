import log
from datetime import datetime


status = log.Log()
status.log

now = datetime.now()
class Auth:
    __data_user = {'username': 'root', 'password': 'secret'}
    def __init__(self):
        self._username = ""
        self._status = False
        self._guest = False
        self.current = now.strftime("%d-%m-%Y %H:%M:%S")

    def login(self, username="", password=""):
        self.validate(username,password)
        
    def validate(self, username="", password=""):
         if username == self.__data_user["username"] and password == self.__data_user["password"]:
             self._username += username
             self._status = True
             self._guest = False
             self.current = now.strftime("%d-%m-%Y %H:%M:%S")

    def logout(self):
        self._username += ""
        self._status = False
        self._guest = True

    def id(self):
        return id(self.login)

    def check(self):
        if self._status == True:
            return self._status
        else:
            return self._status
            
    def guest(self):
        return self._guest
    
    def user(self):
        print('Nama : ', self._username)
        print('Status Login : ', self._status)
        print('Status guest : ', self.guest())
        print("Login Id : ", self.id)
        print('Terakhir login :', self.lastLogin())

    def lastLogin(self):
        return self.current

auth = Auth()
auth.login('root', 'secret')      # If valid, user will log in.
auth.user()
auth.validate('root', 'secret')  # Just verify username and password without log in.
auth.user()            # Get information about current logged in user.
auth.check()           # Will returns true if user already logged in.
auth.logout()          # Log out the current logged in user.
auth.guest()           # Will returns true if user not logged in.
auth.lastLogin()       # Get information when the user last logged in.
